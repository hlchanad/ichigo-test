/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  globals: {
    'ts-jest': {
      tsconfig: '<rootDir>/tsconfig.json',
      isolatedModules: true,
      diagnostics: false,
    },
  },
  moduleFileExtensions: ['js', 'json', 'ts'],
  testRegex: '\\.(e2e-)?spec\\.ts$',
  transform: {
    '^.+\\.(j|t)s$': 'ts-jest',
  },
  testPathIgnorePatterns: ['/node_modules/', '/lib/'],
  setupFilesAfterEnv: [],
  moduleNameMapper: {},
};
