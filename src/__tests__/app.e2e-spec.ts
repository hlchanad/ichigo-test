import * as express from 'express';
import * as expressAsyncHandler from 'express-async-handler';
import * as request from 'supertest';
import { createApp } from '../app';
import { HttpException } from '../types';

let app: express.Express;
let router: express.Router;

beforeEach(() => {
  router = express.Router();
  router.get('/', (request: express.Request, response: express.Response) => {
    response.send('Hello World');
  });
  router.get('/error', () => {
    throw new Error("something's wrong");
  });
  router.get('/http-exception', () => {
    throw new HttpException(400, 'bad request');
  });
  router.get(
    '/async-handler-error',
    expressAsyncHandler(async () => {
      throw new Error('async handler error');
    }),
  );

  app = createApp(router);
});

describe('global error handle', () => {
  it('catches all error in route handlers', async () => {
    await request(app)
      .get('/error')
      .expect(500, { error: { message: "something's wrong" } });
  });

  it('catches all error in async route handlers', async () => {
    await request(app)
      .get('/async-handler-error')
      .expect(500, { error: { message: 'async handler error' } });
  });

  it('catches HttpException in route handlers', async () => {
    await request(app)
      .get('/http-exception')
      .expect(400, { error: { message: 'bad request' } });
  });
});

describe('404 route fallback', () => {
  it('reaches actual route', async () => {
    await request(app).get('/').expect(200, 'Hello World');
  });

  it('throws 404 error', async () => {
    await request(app)
      .get('/no-such-route')
      .expect(404, { error: { message: 'no such route' } });
  });
});
