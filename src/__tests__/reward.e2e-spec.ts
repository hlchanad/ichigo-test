import * as express from 'express';
import * as faker from 'faker';
import * as MockDate from 'mockdate';
import * as request from 'supertest';
import { createApp } from '../app';
import { RewardController } from '../controllers';
import { RewardDocument, UserDocument } from '../models';
import { router } from '../router';
import { RewardService, UserService } from '../services';

let users: Array<UserDocument>;
let rewards: Array<RewardDocument>;

let userService: UserService;
let rewardService: RewardService;

let rewardController: RewardController;

let app: express.Express;

beforeEach(() => {
  users = [];
  rewards = [];

  userService = new UserService(users);
  rewardService = new RewardService(rewards);

  rewardController = new RewardController(userService, rewardService);

  app = createApp(router(rewardController));
});

describe('GET /users/:userId/rewards', () => {
  it('configures action + url correctly', async () => {
    const userId = faker.random.alphaNumeric(8);

    const response = await request(app).get(
      `/users/${userId}/rewards?at=2022-03-10T12:34:56Z`,
    );

    expect(response.status).not.toBe(404);
  });

  it('returns a week of rewards', async () => {
    const userId = faker.random.alphaNumeric(8);

    const response = await request(app)
      .get(`/users/${userId}/rewards?at=2022-03-10T12:34:56Z`)
      .expect(200);

    // call twice, should have created once
    // -> rewards should have length 7
    await request(app)
      .get(`/users/${userId}/rewards?at=2022-03-10T12:34:56Z`)
      .expect(200);

    expect(response.body.data).toHaveLength(7);
    expect(response.body.data).toEqual([
      {
        availableAt: '2022-03-06T00:00:00.000Z',
        expiresAt: '2022-03-07T00:00:00.000Z',
      },
      {
        availableAt: '2022-03-07T00:00:00.000Z',
        expiresAt: '2022-03-08T00:00:00.000Z',
      },
      {
        availableAt: '2022-03-08T00:00:00.000Z',
        expiresAt: '2022-03-09T00:00:00.000Z',
      },
      {
        availableAt: '2022-03-09T00:00:00.000Z',
        expiresAt: '2022-03-10T00:00:00.000Z',
      },
      {
        availableAt: '2022-03-10T00:00:00.000Z',
        expiresAt: '2022-03-11T00:00:00.000Z',
      },
      {
        availableAt: '2022-03-11T00:00:00.000Z',
        expiresAt: '2022-03-12T00:00:00.000Z',
      },
      {
        availableAt: '2022-03-12T00:00:00.000Z',
        expiresAt: '2022-03-13T00:00:00.000Z',
      },
    ]);

    expect(rewards).toHaveLength(7);
    expect(rewards).toEqual([
      {
        userId,
        availableAt: new Date('2022-03-06T00:00:00.000Z'),
        expiresAt: new Date('2022-03-07T00:00:00.000Z'),
      },
      {
        userId,
        availableAt: new Date('2022-03-07T00:00:00.000Z'),
        expiresAt: new Date('2022-03-08T00:00:00.000Z'),
      },
      {
        userId,
        availableAt: new Date('2022-03-08T00:00:00.000Z'),
        expiresAt: new Date('2022-03-09T00:00:00.000Z'),
      },
      {
        userId,
        availableAt: new Date('2022-03-09T00:00:00.000Z'),
        expiresAt: new Date('2022-03-10T00:00:00.000Z'),
      },
      {
        userId,
        availableAt: new Date('2022-03-10T00:00:00.000Z'),
        expiresAt: new Date('2022-03-11T00:00:00.000Z'),
      },
      {
        userId,
        availableAt: new Date('2022-03-11T00:00:00.000Z'),
        expiresAt: new Date('2022-03-12T00:00:00.000Z'),
      },
      {
        userId,
        availableAt: new Date('2022-03-12T00:00:00.000Z'),
        expiresAt: new Date('2022-03-13T00:00:00.000Z'),
      },
    ]);
  });
});

describe('PATCH /users/:userId/rewards/:availableAt/redeem', () => {
  beforeEach(() => {
    MockDate.set('2022-03-12T12:34:56Z');

    users.push({ id: faker.random.alphaNumeric(8) });
    rewards.push({
      userId: users[0].id,
      availableAt: new Date('2022-03-12T00:00:00Z'),
      expiresAt: new Date('2022-03-13T00:00:00Z'),
    });
  });

  afterEach(() => {
    MockDate.reset();
  });

  it('configures action + url correctly', async () => {
    const userId = faker.random.alphaNumeric(8);
    const availableAt = faker.date.recent().toISOString();

    const response = await request(app).patch(
      `/users/${userId}/rewards/${availableAt}/redeem`,
    );

    expect(response.status).not.toBe(404);
  });

  it('redeems reward', async () => {
    const userId = users[0].id;
    const availableAt = '2022-03-12T00:00:00Z';

    await request(app)
      .patch(`/users/${userId}/rewards/${availableAt}/redeem`)
      .expect(204);

    expect(rewards[0].redeemedAt).toBeDefined();
    expect(rewards[0].redeemedAt).toEqual(expect.any(Date));
  });
});
