import * as express from 'express';
import { NextFunction, Request, Response } from 'express';
import { HttpException } from './types';

export function createApp(router: express.Router): express.Express {
  const app = express();

  app.use(router);

  app.use(() => {
    throw new HttpException(404, 'no such route');
  });

  app.use(
    (
      error: Error,
      request: Request,
      response: Response,
      next: NextFunction,
    ) => {
      const status = error instanceof HttpException ? error.status : 500;
      const message = error.message;

      return response.status(status).json({ error: { message } });
    },
  );

  return app;
}
