import { getMockReq, getMockRes } from '@jest-mock/express';
import * as faker from 'faker';
import { RewardDocument, UserDocument } from '../../models';
import { RewardService, UserService } from '../../services';
import { HttpException } from '../../types';
import { RewardController } from '../reward.controller';

let userService: {
  findUser: jest.MockInstance<
    ReturnType<typeof UserService.prototype.findUser>,
    Parameters<typeof UserService.prototype.findUser>
  >;
  createUser: jest.MockInstance<
    ReturnType<typeof UserService.prototype.createUser>,
    Parameters<typeof UserService.prototype.createUser>
  >;
};
let rewardService: {
  listOrCreate: jest.MockInstance<
    ReturnType<typeof RewardService.prototype.listOrCreate>,
    Parameters<typeof RewardService.prototype.listOrCreate>
  >;
  findReward: jest.MockInstance<
    ReturnType<typeof RewardService.prototype.findReward>,
    Parameters<typeof RewardService.prototype.findReward>
  >;
  redeemReward: jest.MockInstance<
    ReturnType<typeof RewardService.prototype.redeemReward>,
    Parameters<typeof RewardService.prototype.redeemReward>
  >;
};
let rewardController: RewardController;

beforeEach(() => {
  userService = {
    findUser: jest.fn<
      ReturnType<typeof UserService.prototype.findUser>,
      Parameters<typeof UserService.prototype.findUser>
    >(),
    createUser: jest.fn<
      ReturnType<typeof UserService.prototype.createUser>,
      Parameters<typeof UserService.prototype.createUser>
    >(),
  };

  rewardService = {
    listOrCreate: jest.fn<
      ReturnType<typeof RewardService.prototype.listOrCreate>,
      Parameters<typeof RewardService.prototype.listOrCreate>
    >(),
    findReward: jest.fn<
      ReturnType<typeof RewardService.prototype.findReward>,
      Parameters<typeof RewardService.prototype.findReward>
    >(),
    redeemReward: jest.fn<
      ReturnType<typeof RewardService.prototype.redeemReward>,
      Parameters<typeof RewardService.prototype.redeemReward>
    >(),
  };

  rewardController = new RewardController(
    userService as unknown as UserService,
    rewardService as unknown as RewardService,
  );
});

describe('listRewards()', () => {
  it('throws error if invalid date', async () => {
    const request = getMockReq({ query: { at: 'invalid date' } });
    const { res: response } = getMockRes();

    await expect(() =>
      rewardController.listRewards(request, response),
    ).rejects.toThrow(new HttpException(400, 'invalid date'));
  });

  it('creates user if user not found', async () => {
    const userId = faker.random.alphaNumeric(8);

    userService.findUser.mockResolvedValue(null);
    userService.createUser.mockResolvedValue({ id: userId });
    rewardService.listOrCreate.mockResolvedValue([]);

    const request = getMockReq({
      query: { at: '2022-03-11T12:34:56Z' },
      params: { userId },
    });
    const { res: response } = getMockRes();

    await rewardController.listRewards(request, response);

    expect(userService.createUser).toBeCalled();
    expect(userService.createUser).toBeCalledWith({ id: userId });
  });

  it('calls rewardService.listOrCreate()', async () => {
    const userId = faker.random.alphaNumeric(8);
    const date = '2022-03-11T12:34:56Z';

    userService.findUser.mockResolvedValue({ id: userId });
    rewardService.listOrCreate.mockResolvedValue([]);

    const request = getMockReq({
      query: { at: date },
      params: { userId },
    });
    const { res: response } = getMockRes();

    await rewardController.listRewards(request, response);

    expect(rewardService.listOrCreate).toBeCalled();
    expect(rewardService.listOrCreate).toBeCalledWith(userId, new Date(date));
  });
});

describe('redeemReward', () => {
  let userId: string;

  beforeEach(() => {
    userId = faker.random.alphaNumeric(8);
  });

  it('throws error if invalid date', async () => {
    const request = getMockReq({ params: { availableAt: 'invalid date' } });
    const { res: response } = getMockRes();

    await expect(() =>
      rewardController.redeemReward(request, response),
    ).rejects.toThrow(new HttpException(400, 'invalid date'));
  });

  it('throws error if user not found', async () => {
    userService.findUser.mockResolvedValue(null);

    const request = getMockReq({
      params: { userId, availableAt: '2022-03-11T12:34:56Z' },
    });
    const { res: response } = getMockRes();

    await expect(() =>
      rewardController.redeemReward(request, response),
    ).rejects.toThrow(new HttpException(400, 'user not found'));
  });

  it('throws error if reward not found', async () => {
    userService.findUser.mockResolvedValue({} as UserDocument);
    rewardService.findReward.mockResolvedValue(null);

    const request = getMockReq({
      params: { userId, availableAt: '2022-03-11T12:34:56Z' },
    });
    const { res: response } = getMockRes();

    await expect(() =>
      rewardController.redeemReward(request, response),
    ).rejects.toThrow(new HttpException(400, 'reward not found'));
  });

  it('throws error if reward expired', async () => {
    userService.findUser.mockResolvedValue({} as UserDocument);
    rewardService.findReward.mockResolvedValue({
      expiresAt: faker.date.past(),
    } as RewardDocument);

    const request = getMockReq({
      params: { userId, availableAt: '2022-03-11T12:34:56Z' },
    });
    const { res: response } = getMockRes();

    await expect(() =>
      rewardController.redeemReward(request, response),
    ).rejects.toThrow(new HttpException(400, 'This reward is already expired'));
  });

  it('calls rewardService.redeemReward()', async () => {
    const reward = {} as RewardDocument;

    userService.findUser.mockResolvedValue({} as UserDocument);
    rewardService.findReward.mockResolvedValue(reward);

    const request = getMockReq({
      params: { userId, availableAt: '2022-03-11T12:34:56Z' },
    });
    const { res: response } = getMockRes();

    await rewardController.redeemReward(request, response);

    expect(rewardService.redeemReward).toBeCalled();
    expect(rewardService.redeemReward).toBeCalledWith(reward);
  });
});
