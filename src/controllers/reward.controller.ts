import { Request, Response } from 'express';
import { isValidDate } from '../helpers';
import { RewardService, UserService } from '../services';
import { HttpException } from '../types';

export class RewardController {
  constructor(
    private readonly userService: UserService,
    private readonly rewardService: RewardService,
  ) {}

  async listRewards(request: Request, response: Response) {
    if (!isValidDate(request.query.at as string)) {
      throw new HttpException(400, 'invalid date');
    }

    const userId = request.params.userId as string;
    const date = new Date(request.query.at as string);

    let user = await this.userService.findUser(userId);

    if (!user) {
      user = await this.userService.createUser({ id: userId });
    }

    const rewards = await this.rewardService.listOrCreate(user.id, date);

    return response.json({
      data: rewards.map((reward) => ({
        availableAt: reward.availableAt.toISOString(),
        redeemedAt: reward.redeemedAt?.toISOString(),
        expiresAt: reward.expiresAt.toISOString(),
      })),
    });
  }

  async redeemReward(request: Request, response: Response) {
    if (!isValidDate(request.params.availableAt as string)) {
      throw new HttpException(400, 'invalid date');
    }

    const userId = request.params.userId as string;
    const availableAt = new Date(request.params.availableAt as string);

    const user = await this.userService.findUser(userId);

    if (!user) {
      throw new HttpException(400, 'user not found');
    }

    const reward = await this.rewardService.findReward(userId, availableAt);

    if (!reward) {
      throw new HttpException(400, 'reward not found');
    }

    if (reward.expiresAt < new Date()) {
      throw new HttpException(400, 'This reward is already expired');
    }

    await this.rewardService.redeemReward(reward);

    response.status(204).send();
  }
}
