import { cloneDeep } from 'lodash';
import { Reward, RewardDocument } from '../models';
import { toEndOfWeek, toStartOfWeek } from '../helpers';

export class RewardService {
  constructor(private readonly rewards: Array<RewardDocument>) {}

  async listOrCreate(
    userId: string,
    date: Date,
  ): Promise<Array<RewardDocument>> {
    const startOfWeek = toStartOfWeek(date);
    const endOfWeek = toEndOfWeek(date);

    const rewards = this.rewards
      .filter(
        (reward) =>
          reward.userId === userId &&
          reward.availableAt >= startOfWeek &&
          reward.availableAt <= endOfWeek,
      )
      .sort(
        (reward1, reward2) =>
          reward1.availableAt.getTime() - reward2.availableAt.getTime(),
      );

    if (rewards.length <= 0) {
      const current = new Date(startOfWeek);
      while (current <= endOfWeek) {
        const expiresAt = new Date(current);
        expiresAt.setDate(expiresAt.getDate() + 1);

        const reward = await this.createReward({
          userId,
          availableAt: new Date(current),
          expiresAt,
        });
        rewards.push(reward);

        current.setDate(current.getDate() + 1);
      }
    }

    return cloneDeep(rewards);
  }

  async createReward(reward: Reward): Promise<RewardDocument> {
    const rewardDocument = { ...reward };
    this.rewards.push(rewardDocument);
    return cloneDeep(rewardDocument);
  }

  async findReward(
    userId: string,
    availableAt: Date,
  ): Promise<RewardDocument | null> {
    const reward = this.rewards.find(
      (reward) =>
        reward.userId === userId &&
        reward.availableAt.getTime() === availableAt.getTime(),
    );
    return reward ? cloneDeep(reward) : null;
  }

  async redeemReward({
    userId,
    availableAt,
  }: RewardDocument): Promise<RewardDocument> {
    const reward = this.rewards.find(
      (reward) =>
        reward.userId === userId &&
        reward.availableAt.getTime() === availableAt.getTime(),
    );
    reward.redeemedAt = new Date();
    return cloneDeep(reward);
  }
}
