import * as faker from 'faker';
import { UserDocument } from '../../models';
import { UserSeed } from '../../models/__tests__/user.seed';
import { UserService } from '../user.service';

let users: UserDocument[];
let userService: UserService;

describe('findUser()', () => {
  beforeEach(() => {
    users = [UserSeed(), UserSeed()];
    userService = new UserService(users);
  });

  it('returns user', async () => {
    const result = await userService.findUser(users[1].id);
    expect(result).toEqual(expect.objectContaining({ id: users[1].id }));
  });
});

describe('createUser()', () => {
  beforeEach(() => {
    users = [];
    userService = new UserService(users);
  });

  it('creates user', async () => {
    const user = { id: faker.random.alphaNumeric(8) };
    await userService.createUser(user);
    expect(users).toHaveLength(1);
  });
});
