import * as faker from 'faker';
import { RewardDocument } from '../../models';
import { RewardSeed } from '../../models/__tests__/reward.seed';
import { RewardService } from '../reward.service';

let userId: string;
let rewards: RewardDocument[];
let rewardService: RewardService;

beforeEach(() => {
  userId = faker.random.alphaNumeric(8);
  rewards = [
    RewardSeed({
      userId,
      availableAt: new Date('2022-03-11T00:00:00Z'),
      expiresAt: new Date('2022-03-12T00:00:00Z'),
    }),
    RewardSeed({
      userId,
      availableAt: new Date('2022-03-12T00:00:00Z'),
      expiresAt: new Date('2022-03-13T00:00:00Z'),
    }),
    RewardSeed({
      userId,
      availableAt: new Date('2022-03-13T00:00:00Z'),
      expiresAt: new Date('2022-03-14T00:00:00Z'),
    }),
  ];
  rewardService = new RewardService(rewards);
});

describe('listOrCreate()', () => {
  it('returns rewards in the same week', async () => {
    const result = await rewardService.listOrCreate(
      userId,
      new Date('2022-03-11T12:34:56Z'),
    );

    expect(result).toHaveLength(2);
    expect(result).toEqual([
      expect.objectContaining({
        availableAt: new Date('2022-03-11T00:00:00Z'),
        expiresAt: new Date('2022-03-12T00:00:00Z'),
      }),
      expect.objectContaining({
        availableAt: new Date('2022-03-12T00:00:00Z'),
        expiresAt: new Date('2022-03-13T00:00:00Z'),
      }),
    ]);
  });

  it('creates reward for whole week if not found', async () => {
    const result = await rewardService.listOrCreate(
      userId,
      new Date('2022-03-04T12:34:56Z'),
    );

    expect(rewards).toHaveLength(10);

    expect(result).toHaveLength(7);
    expect(result).toEqual([
      expect.objectContaining({
        availableAt: new Date('2022-02-27T00:00:00Z'),
        expiresAt: new Date('2022-02-28T00:00:00Z'),
      }),
      expect.objectContaining({
        availableAt: new Date('2022-02-28T00:00:00Z'),
        expiresAt: new Date('2022-03-01T00:00:00Z'),
      }),
      expect.objectContaining({
        availableAt: new Date('2022-03-01T00:00:00Z'),
        expiresAt: new Date('2022-03-02T00:00:00Z'),
      }),
      expect.objectContaining({
        availableAt: new Date('2022-03-02T00:00:00Z'),
        expiresAt: new Date('2022-03-03T00:00:00Z'),
      }),
      expect.objectContaining({
        availableAt: new Date('2022-03-03T00:00:00Z'),
        expiresAt: new Date('2022-03-04T00:00:00Z'),
      }),
      expect.objectContaining({
        availableAt: new Date('2022-03-04T00:00:00Z'),
        expiresAt: new Date('2022-03-05T00:00:00Z'),
      }),
      expect.objectContaining({
        availableAt: new Date('2022-03-05T00:00:00Z'),
        expiresAt: new Date('2022-03-06T00:00:00Z'),
      }),
    ]);
  });
});

describe('findReward()', () => {
  it('returns reward', async () => {
    const reward = await rewardService.findReward(
      userId,
      new Date('2022-03-11T00:00:00Z'),
    );
    expect(reward).toBeDefined();
    expect(reward).toMatchObject(rewards[0]);
  });

  it('returns null if not found', async () => {
    const reward = await rewardService.findReward(
      'dummy',
      new Date('2022-03-11T00:00:00Z'),
    );
    expect(reward).toBeNull();
  });
});

describe('redeemReward()', () => {
  it('sets redeemedAt', async () => {
    await rewardService.redeemReward(rewards[1]);

    expect(rewards[1].redeemedAt).toBeDefined();
    expect(rewards[1].redeemedAt).toEqual(expect.any(Date));
  });
});
