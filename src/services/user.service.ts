import { cloneDeep } from 'lodash';
import { UserDocument, User } from '../models';

export class UserService {
  constructor(private readonly users: Array<UserDocument>) {}

  async findUser(id: string): Promise<UserDocument | null> {
    const user = this.users.find((user) => user.id === id);
    return user ? cloneDeep(user) : null;
  }

  async createUser(user: User): Promise<UserDocument> {
    const userDocument = { ...user };
    this.users.push(userDocument);
    return cloneDeep(userDocument);
  }
}
