import { Router } from 'express';
import * as expressAsyncHandler from 'express-async-handler';
import { RewardController } from './controllers';

export function router(rewardController: RewardController): Router {
  const router = Router();

  router.get(
    '/users/:userId/rewards',
    expressAsyncHandler(rewardController.listRewards.bind(rewardController)),
  );

  router.patch(
    '/users/:userId/rewards/:availableAt/redeem',
    expressAsyncHandler(rewardController.redeemReward.bind(rewardController)),
  );

  return router;
}
