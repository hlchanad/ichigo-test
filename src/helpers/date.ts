export function toStartOfWeek(date: Date): Date {
  const result = new Date(date);

  result.setUTCDate(date.getDate() - date.getDay());
  result.setUTCHours(0);
  result.setUTCMinutes(0);
  result.setUTCSeconds(0);
  result.setUTCMilliseconds(0);

  return result;
}

export function toEndOfWeek(date: Date): Date {
  const result = new Date(date);

  result.setUTCDate(date.getDate() + (6 - date.getDay()));
  result.setUTCHours(0);
  result.setUTCMinutes(0);
  result.setUTCSeconds(0);
  result.setUTCMilliseconds(0);

  return result;
}

export function isValidDate(date: string): boolean {
  // @ts-ignore
  return date && new Date(date) !== 'Invalid Date' && !isNaN(new Date(date));
}
