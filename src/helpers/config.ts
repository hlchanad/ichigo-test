import { get } from 'lodash';
import * as AppConfig from '../config';

export function config<T>(path: string, defaultValue?: T): T {
  return get(AppConfig, path, defaultValue);
}
