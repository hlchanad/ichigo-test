import { isValidDate, toEndOfWeek, toStartOfWeek } from '../date';

describe('toStartOfWeek()', () => {
  it('converts to start of week', () => {
    expect(toStartOfWeek(new Date('2022-03-04T12:34:56Z'))).toEqual(
      new Date('2022-02-27T00:00:00Z'),
    );
    expect(toStartOfWeek(new Date('2022-03-10T12:34:56Z'))).toEqual(
      new Date('2022-03-06T00:00:00Z'),
    );
    expect(toStartOfWeek(new Date('2022-03-06T00:00:00Z'))).toEqual(
      new Date('2022-03-06T00:00:00Z'),
    );
  });
});

describe('toEndOfWeek()', () => {
  it('converts to end of week', () => {
    expect(toEndOfWeek(new Date('2022-03-10T12:34:56Z'))).toEqual(
      new Date('2022-03-12T00:00:00Z'),
    );
    expect(toEndOfWeek(new Date('2022-03-06T00:00:00Z'))).toEqual(
      new Date('2022-03-12T00:00:00Z'),
    );
    expect(toEndOfWeek(new Date('2022-03-31T00:00:00Z'))).toEqual(
      new Date('2022-04-02T00:00:00Z'),
    );
  });
});

describe('isValidDate()', () => {
  it('returns true if valid', () => {
    expect(isValidDate('2022-03-12T00:00:00Z')).toBeTruthy();
  });

  it('returns false if valid', () => {
    expect(isValidDate('abc')).toBeFalsy();
  });
});
