export interface User {
  id: string;
}

export type UserDocument = User;
