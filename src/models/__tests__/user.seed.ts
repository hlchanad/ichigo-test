import { User, UserDocument } from '../user.model';
import * as faker from 'faker';

export function UserSeed(args?: Partial<User>): UserDocument {
  return {
    id: faker.random.alphaNumeric(8),
    ...args,
  };
}
