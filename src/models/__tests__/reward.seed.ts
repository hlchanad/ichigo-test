import * as faker from 'faker';
import { Reward, RewardDocument } from '../reward.model';

export function RewardSeed(args?: Partial<Reward>): RewardDocument {
  const date = faker.date.recent();
  date.setUTCHours(0);
  date.setUTCMinutes(0);
  date.setUTCSeconds(0);
  date.setUTCMilliseconds(0);

  const expiresAt = new Date(date);
  expiresAt.setDate(expiresAt.getDate() + 1);

  return {
    userId: faker.random.alphaNumeric(8),
    availableAt: date,
    expiresAt,
    redeemedAt: null,
    ...args,
  };
}
