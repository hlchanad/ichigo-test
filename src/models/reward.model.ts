export interface Reward {
  userId: string;
  availableAt: Date;
  redeemedAt?: Date;
  expiresAt: Date;
}

export type RewardDocument = Reward;
