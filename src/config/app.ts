export const app = {
  environment: process.env.ENVIRONMENT ?? 'development',
  port: process.env.PORT ?? 4000,
};
