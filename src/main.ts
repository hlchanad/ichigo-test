import { createApp } from './app';
import { RewardController } from './controllers';
import { config } from './helpers';
import { RewardDocument, UserDocument } from './models';
import { router } from './router';
import { RewardService, UserService } from './services';

// Dependencies
const users: Array<UserDocument> = [];
const rewards: Array<RewardDocument> = [];

const userService = new UserService(users);
const rewardService = new RewardService(rewards);

const rewardController = new RewardController(userService, rewardService);

// App
const app = createApp(router(rewardController));

const port = config('app.port');
app.listen(port, () => console.log(`Server listening on port ${port}`));
