FROM node:16-alpine3.15 as builder

WORKDIR /app

COPY ./package* ./

RUN npm ci

COPY . .

RUN npm run build

FROM node:16-alpine3.15

WORKDIR /app

COPY ./package* ./

RUN npm ci --production --ignore-scripts

COPY --from=builder /app/dist ./dist

CMD ["npm", "run", "start:prod"]
