# Ichigo Test

It is a simple reward service for fetching daily rewards and redeeming.

---

## Requirements

1. [Docker](https://docs.docker.com/install/)
   
2. [Docker Compose](https://docs.docker.com/compose/install/)


## How to run it?

1. Clone the repository:
```shell
$ git clone git@bitbucket.org:hlchanad/ichigo-test.git
```

2. Build the application
```shell
$ docker-compose build
```

3. (Optional) Run test suite
```shell
$ docker-compose run --rm server-test
```

4. Run the server
```shell
$ docker-compose up server
```

---

## Design of the service

#### Dependency Injection

- Loose couple component
- invert the control
- easy for test cases

#### Using async/ await in service files
- mimic fetching data from database
- easy to modify `service` without updating `controller`

---

## APIs

#### Fetch rewards in current week
It pre-creates user and rewards if not found.

Request:
```
GET /users/:userId/rewards?at=:date
```

Response: 
```json
{
  "data": [
    { "availableAt": "2020-03-15T00:00:00Z", "redeemedAt": null, "expiresAt": "2020-03-16T00:00:00Z" },
    { "availableAt": "2020-03-16T00:00:00Z", "redeemedAt": null, "expiresAt": "2020-03-17T00:00:00Z" },
    { "availableAt": "2020-03-17T00:00:00Z", "redeemedAt": null, "expiresAt": "2020-03-18T00:00:00Z" },
    { "availableAt": "2020-03-18T00:00:00Z", "redeemedAt": null, "expiresAt": "2020-03-19T00:00:00Z" },
    { "availableAt": "2020-03-19T00:00:00Z", "redeemedAt": null, "expiresAt": "2020-03-20T00:00:00Z" },
    { "availableAt": "2020-03-20T00:00:00Z", "redeemedAt": null, "expiresAt": "2020-03-21T00:00:00Z" },
    { "availableAt": "2020-03-21T00:00:00Z", "redeemedAt": null, "expiresAt": "2020-03-22T00:00:00Z" }
  ]
}
```

#### Redeem reward
It redeems the reward of user (return error if user/ reward not found).

Request:
```
PATCH /users/:userId/rewards/:date/redeem
```

Response: 
```
(no content)
```
